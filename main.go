package main

import (
	"sockets/client"
	"sockets/server"
	"sockets/util"
	"time"
)

// 程序入口函数
func main() {
	// 将sync.WaitGroup类型值, 计数器差值设置2,
	// 因为这里要同时等待服务端程序和客户端程序执行结束
	util.Wg.Add(2)

	// 为了服务端程序和客户端程序能够并发运行,
	// 分别使用 go 语句执行 serverGo函数和 clientGo函数,
	// 并且, 客户端程序运行的时机在服务端程序开始运行并已准备好接收新连接之后.
	// 因此, 让这两个go函数的执行有一点时间间隔, 这里500毫秒的时间间隔是足够的.
	go server.Server()
	time.Sleep(500 * time.Millisecond)
	go client.Client(1)
	util.Wg.Wait()
}
