package client

import (
	"fmt"
	"io"
	"math/rand"
	"net"
	"sockets/util"
	"time"
)

// Socket 客户端程序
// @param id int 客户端编号
func Client(id int) {
	// 使用同步等待组计数器值减一
	defer util.Wg.Done()

	// -----------------------------------------------------------------
	// 与服务器建立连接
	// -----------------------------------------------------------------
	// 发送连接建立申请
	conn, err := net.DialTimeout(util.ServerNetwork, util.ServerAddress, 2*time.Second)

	// 发送连接建立申请, 可能发生的错误检测
	if err != nil {
		// 打印客户端日志 - 发送连接建立申请失败
		util.PrintClientLog(id, "Dial Error: %s", err)

		// 到了这里下面也就不用再执行了, 直接退出
		return
	}

	// 延迟函数
	defer conn.Close()

	// 打印客户端日志 - 发送连接建立申请成功
	util.PrintClientLog(id, "Connected to server. (remote address: %s, local address: %s)", conn.RemoteAddr(), conn.LocalAddr())

	// 让客户端睡眠 200 毫秒纯属是为了两端程序记录的日志看起来更清晰一些
	time.Sleep(200 * time.Millisecond)

	// -----------------------------------------------------------------
	// 发送请求数据
	// -----------------------------------------------------------------
	// 这里把每个客户端发送的请求数据块的数量定为5个
	requestNumber := 5

	// 为了满足检验服务器程序响应速度的需求, 还要在发送和接收操作开始前设置一下超时时间(5毫秒)
	conn.SetDeadline(time.Now().Add(5 * time.Millisecond))

	// 发送数据块
	for i := 0; i < requestNumber; i++ {
		// 使用标准库代码包rand中的函数Int31可以随机生成一个int32类型值
		req := rand.Int31()

		// 写入到连接发送缓冲区
		n, err := util.Write(conn, fmt.Sprintf("%d", req))

		// 写入到连接发送缓冲区, 可能存在的错误检测
		if err != nil {
			// 打印客户端日志 - 写入到连接发送缓冲区错误
			util.PrintClientLog(id, "Write Error: %s", err)

			// 退出当前循环写入数据操作, 并进行到下一次的数据写入操作
			continue
		}

		// 打印客户端日志 - 写入到连接发送缓冲区成功
		util.PrintClientLog(id, "Sent request (written %d bytes): %d.", n, req)
	}

	// -----------------------------------------------------------------
	// 接收响应数据块
	// -----------------------------------------------------------------
	for j := 0; j < requestNumber; j++ {
		// 从连接中(Socket的接收缓冲区)读取一段以数据定界符为结尾的数据块
		responseMessage, err := util.Read(conn)

		// 从连接中(Socket的接收缓冲区)读取一段以数据定界符为结尾的数据块, 可能存在的错误检测
		if err != nil {
			if err == io.EOF {
				// 打印客户端日志 - 连接由数据发送方关闭(服务端)
				util.PrintClientLog(id, "The connection is closed by author side.")
			} else {
				// 打印客户端日志 - 读取错误
				util.PrintClientLog(id, "Read Error: %s", err)
			}
			break
		}

		// 从连接中(Socket的接收缓冲区)读取一段以数据定界符为结尾的数据块, 接收成功
		util.PrintClientLog(id, "Received response: %s.", responseMessage)
	}
}
