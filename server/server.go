package server

import (
	"fmt"
	"io"
	"net"
	"os"
	"sockets/util"
	"time"
)

// Socket 服务端程序
func Server() {
	// -----------------------------------------------------------------
	// 获取 Socket 监听器
	// -----------------------------------------------------------------
	// 创建监听器
	var listener net.Listener
	listener, err := net.Listen(util.ServerNetwork, util.ServerAddress)

	// 创建监听器, 可能出现的错误检测
	if err != nil {
		// 打印服务端日志 - 获取监听器失败
		util.PrintServerLog("Listen Error:%s", err)

		// 到了这里下面也就不用再执行了, 直接退出
		return
	}

	// 在外围函数 `serverGo` 执行结束前关闭监听器, 而 defer 后的函数称为延迟函数
	defer listener.Close()

	// 打印服务端日志 - 获取到监听器
	util.PrintServerLog("Got listener for the server. (local address: %s)", listener.Addr())

	// -----------------------------------------------------------------
	// 等待客户端连接
	// -----------------------------------------------------------------
	for {
		// 接受等待客户端连接, 阻塞直至新连接到来
		conn, err := listener.Accept()

		// 接受等待客户端连接, 阻塞直至新连接到来, 可能出错的错误检测
		if err != nil {
			// 打印服务端日志 - 获取连接等待失败
			util.PrintServerLog("Accept error: %s", err)
			continue
		}

		// 打印服务端日志 - 获取连接等待成功(与客户端应用程序建立连接)
		util.PrintServerLog("Established a connection with a client application. (remote address: %s)", conn.RemoteAddr())

		// 并发执行连接处理
		go handleConn(conn)
	}
}

// 并发处理连接
func handleConn(conn net.Conn) {
	// 当外围函数执行结束时, 应该把连接关闭
	// 它执行结束可能是由于主体已经执行结束, 也可能是若此代码引发了一个运行时恐慌。
	// 不论怎样, 把当前连接及时关掉都是一件很重要的事情
	defer func() {
		// 服务端程序主动关闭连接
		conn.Close()

		// 使用同步等待组计数器值减一
		util.Wg.Done()
	}()

	for {
		// 设置当前Socket连接读取数据超时时间
		conn.SetReadDeadline(time.Now().Add(10 * time.Second))

		// 从连接中(Socket的接收缓冲区)读取一段以数据定界符为结尾的数据块
		strReq, err := util.Read(conn)

		// 从连接中(Socket的接收缓冲区)读取一段以数据定界符为结尾的数据块
		// 可能出现的错误检测
		if err != nil {
			if err == io.EOF {
				// 打印服务端日志 - 连接由数据发送方关闭
				util.PrintServerLog("The connection is closed by author side.")
			} else {
				// 打印服务端日志 - 读取错误
				util.PrintServerLog("Read Error: %s.", err)
				os.Exit(0)
			}
			break
		}

		// 打印服务端日志 - 接收到网络连接读取数据
		util.PrintServerLog("Received wrnerwu request: %s.", strReq)

		// 将数字字符串转换为32位整型
		intReq, err := util.StrToInt32(strReq)

		// 将数字字符串转换为32位整型, 可能发生的错误检测
		if err != nil {
			// 写入到连接发送缓冲区
			i, err := util.Write(conn, err.Error())

			// 打印服务端日志 - 客户端发送错误消息(以及客户端收到的响应数据的字节个数字, 及错误信息)
			util.PrintServerLog("Sent error message(written %d bytes): 【%s.】", i, err)

			// 退出当前循环读取数据操作, 并进行到下一次的数据读取操作
			continue
		}

		// 计算整数的立方根
		floatResp := util.Cbrt(intReq)

		// 构建响应信息
		responseMessage := fmt.Sprintf("The cube root of %d is %f.", intReq, floatResp)

		// 写入响应数据到连接发送缓冲区
		n, err := util.Write(conn, responseMessage)

		// 写入响应数据到连接发送缓冲区, 可能出现的错误检测
		if err != nil {
			// 打印服务端日志 - 写入响应数据到连接发送缓冲区错误
			util.PrintServerLog("Write Error: %s.", err)
		}

		// 打印服务端日志 - 写入响应数据到连接发送缓冲区成功
		util.PrintServerLog("Sent response (written %d bytes): %s.", n, responseMessage)
	}
}
