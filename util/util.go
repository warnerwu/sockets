package util

import (
	"bytes"
	"fmt"
	"math"
	"net"
	"strconv"
	"strings"
	"sync"
)

// 常量组定义
const (
	// 服务网络协议
	ServerNetwork = "tcp"

	// 服务网络地址
	ServerAddress = "127.0.0.1:8085"

	// 消息边界(数据边界)字节定界符
	Delimiter = '\t'
)

// 初始化同步等待组类型值
var Wg sync.WaitGroup

// 打印日志
// @param role 		string 	打印日志角色
// @param sn 		int 	次数
// @param format 	string 	格式化字符串
// @param args 		int 	日志参数
func PrintLog(role string, sn int, format string, args ...interface{}) {
	if !strings.HasSuffix(format, "\n") {
		format += "\n"
	}
	fmt.Printf("%s[%d]: %s", role, sn, fmt.Sprintf(format, args...))
}

// 打印服务端日志
// @param format 	string 	格式化字符串
// @param args 		int 	日志参数
func PrintServerLog(format string, args ...interface{}) {
	PrintLog("Server", 0, format, args...)
}

// 打印客户端日志
// @param sn 		int 	次数
// @param format 	string 	格式化字符串
// @param args 		int 	日志参数
func PrintClientLog(sn int, format string, args ...interface{}) {
	PrintLog("Client", sn, format, args...)
}

// 从连接中(Socket的接收缓冲区)读取一段以数据定界符为结尾的数据块
// @param conn net.Conn 网络连接
// @return string, error 读取到数据块, 读取时可以产生的错误
func Read(conn net.Conn) (string, error) {
	// 长度为的字节切片
	readBytes := make([]byte, 1)

	// 暂存当前数据块
	var buffer bytes.Buffer

	for {
		// 从Socket的接收缓冲区读取数据, 每次只读取长度为1个字节
		// 防止从Socket的接收缓冲区读出多余的数据从而对后续的读取操作造成影响
		_, err := conn.Read(readBytes)

		// 从Socket的接收缓冲区读取数据, 错误类型值判断
		if err != nil {
			return "", err
		}

		// 获取到每一次读取到的字节数据
		readByte := readBytes[0]

		// 进行判断是定界符
		// 如果不是就继续读取下一个字节数据, 否则就停止读取并返回结果
		if readByte == Delimiter {
			break
		}

		// 写入到数据读取缓存器
		buffer.WriteByte(readByte)
	}

	// 返回由定界符分割的数据块
	return buffer.String(), nil
}

// 写入到连接发送缓冲区
// @param 	str 		string 	待转数字字符串
// @return 	int, error			写入到连接发送缓冲区字节个数, 错误值
func Write(conn net.Conn, content string) (int, error) {
	// 初始化缓冲区
	var buffer bytes.Buffer

	// 写入到缓冲区字符串
	buffer.WriteString(content)

	// 写入到缓冲区定界符字节
	buffer.WriteByte(Delimiter)

	// 写入发送缓冲区字节数据
	return conn.Write(buffer.Bytes())
}

// 将数字字符串转换为32位整型
// @param 	str 				string 	待转数字字符串
// @return 	int32, error			 	转换数字字符串后的32位整型数字, 错误值
func StrToInt32(str string) (int32, error) {
	// 转换字符串到10进制整型数字
	num, err := strconv.ParseInt(str, 10, 0)

	// 转换字符串到10进制整型数字, 错误检测
	if err != nil {
		return 0, fmt.Errorf("\"%s\" is not integer", str)
	}

	// 如果比最int32大或比最小int32小, 则说明这个字符串转换为数字的结果肯定不是int32类型数字
	if num > math.MaxInt32 || num < math.MinInt32 {
		return 0, fmt.Errorf("%d is not 32-int integer", num)
	}

	// 返回int32类型整型数字, 以及错误值nil
	return int32(num), nil
}

// 转换32位整型数字到float64位浮点型数字
func Cbrt(param int32) float64 {
	return math.Cbrt(float64(param))
}
